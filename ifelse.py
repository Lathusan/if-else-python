'''
Created on Feb 1, 2021

@author: Lathusan Thurairajah
'''
a = 36
b = 36
total = (a + b)

print("The sum of a + b = " + str(total))

if a > b:
    print("a > b")
elif a == b:
    print("a = b")
else:
    print("a < b")

if a == b: print("a = b")
